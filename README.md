# DevOps Todo Example

This is a todo app to demonstrate a DevOps build, test, and deployment pipeline.

# Provenance

This app is based on three separate repositories:

* [todo-backend-client][client], a Todo web client, which is itself a slightly-modified
  version of the [TodoMVC Architecture Example for backbone.js][todomvc].
  Copyright (c) Addy Osmani, Sindre Sorhus, Pascal Hartig, Stephen Sawchuk, Pete Hodgson.
* [todo-backend-js-spec][spec], a set of Mocha and Chai tests for a Todo app backend.
* [todo-backend-express][back], a Todo backend implementation that uses Node, Express, and
  PostgreSQL. Copyright (c) Dan Tao.

[client]:https://github.com/TodoBackend/todo-backend-client
[todomvc]:https://github.com/tastejs/todomvc/tree/gh-pages/architecture-examples/backbone/js
[spec]:https://github.com/TodoBackend/todo-backend-js-spec
[back]:https://github.com/dtao/todo-backend-express


These samples are taken from Alan Hohn and Packt learning courses.
